Progressive web app with simple list

Example on branch master includes redux and firebase.
Example on branch no-redux includes firebase.
Example on branch no-firebase includes neither firebase or redux.

Command|Description
--- | ---
*npm start*|Start application with hot reloading enabled
*npm run build*|Minified build with offline capabilities
*npm test*|run tests
*firebase serve*|Serve build locally
*firebase deploy*|Deploy build to firebase
