export const logInAction = (user) => ({
	type: 'LOGGED_IN',
    user: user
})

export const setUserData = (userData) => ({
    type: 'SET_USERDATA',
    userData: userData
})

export const setErrorMessage = (errorMessage) => ({
    type: 'SET_ERROR',
    errorMessage: errorMessage
})

export const clearErrorMessage = () => ({
    type: 'SET_ERROR',
    errorMessage: null
})