const error = (state, action) => {
  if (typeof state === 'undefined') {
    return null
  }
  switch (action.type) {
    case 'SET_ERROR':
      if (action.errorMessage) {
        console.log("TEST", action.errorMessage)
        return action.errorMessage
      }
      else {
        return null
      }
      
    default:
      return state
  }
}

export default error