import { combineReducers } from 'redux'
import userData from './userData'
import error from './error'
import user from './user'

const store = combineReducers({
  user,
  userData,
  error
})

export default store