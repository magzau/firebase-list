const userData = (state, action) => {
  if (typeof state === 'undefined') {
    return null
  }
  switch (action.type) {
    case 'SET_USERDATA':
      return action.userData
    default:
      return state
  }
}

export default userData