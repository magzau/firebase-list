const user = (state, action) => {
  if (typeof state === 'undefined') {
    return null
  }
  switch (action.type) {
    case 'LOGGED_IN':
      return action.user
    default:
      return state
  }
}

export default user