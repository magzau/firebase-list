export default {
    ACTIVE: "active",
    INVITED: "invited",
    ACTIVE_INVITED: "active invited",
    ACTIVE_SHARED: "active shared"
}