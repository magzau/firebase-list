import React, { PropTypes } from 'react'
import { Route, Router, IndexRoute, browserHistory } from 'react-router'

import App from './App'
import Huskeliste from '../components/Huskeliste'
import LogIn from '../components/LogIn'
import NewUser from '../components/NewUser'

const AppRouter = (firebase) => {
    const newFirebase = firebase.firebase

    return (
        <Router history={browserHistory}>
            <Route path="/" component={props => <App firebase={newFirebase} {...props} /> }>
                <IndexRoute component={props => <Huskeliste firebase={newFirebase} {...props} /> }/>
                <Route path="/huskeliste" component={props => <Huskeliste firebase={newFirebase} {...props} /> }/>
                <Route path="/logIn" component={props => <LogIn firebase={newFirebase} {...props} /> }/>
                <Route path="/newUser" component={props => <NewUser firebase={newFirebase} {...props} /> }/>
            </Route>
        </Router>
    )
}

export default AppRouter;