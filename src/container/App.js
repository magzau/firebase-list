import { Layout, Drawer, Navigation, Content, Button } from 'react-mdl'
import { HashRouter, Match, Link, browserHistory } from 'react-router'
import { connect } from 'react-redux'
import React, { PropTypes, Component } from 'react'

import Header from 'grommet/components/Header'
import Title from 'grommet/components/Title'
import Box from 'grommet/components/Box'
import GrommetApp from 'grommet/components/App'
import Search from 'grommet/components/Search'
import Menu from 'grommet/components/Menu'
import MenuIcon from 'grommet/components/icons/base/Menu'

import AnchorLink from '../components/AnchorLink'
import LogIn from '../components/LogIn'
import NewUser from '../components/NewUser'
import Huskeliste from '../components/Huskeliste'
import Error from '../components/Error.js'

import { logOut } from '../api/user'
import { setErrorMessage } from '../actions'

const appname = "Huskeliste"

class App extends Component {
    onLogOutClicked(event) {
        event.preventDefault()
        logOut(this.props.firebase)
    }

    onTitleClicked() {
        this.props.router.push("/")
    }

    componentWillReceiveProps(nextProps) {
        this.props.setErrormessage("")
	}

    render() {
        const user = this.props.user
        const router = this.props.router

        const newUserLink = user === null ? <AnchorLink router={router} href="/newUser">Ny bruker</AnchorLink> : ""
        const logInLink = user === null ? <AnchorLink router={router} href="/logIn">Logg inn</AnchorLink> : ""
        const logOutLink = user !== null ?
            <AnchorLink router={router} href="/" onClick={(event) => {this.onLogOutClicked(event)}}>Logg ut</AnchorLink> : ""

        const title = user ? appname + " " + user.email : appname

        return (
            <GrommetApp>
                <Header>
                    <Title onClick={() => this.onTitleClicked() }>{title}</Title>
                    <Box flex={true}
                        justify='end'
                        direction='row'
                        responsive={false}>
                        <Menu icon={<MenuIcon />}
                            dropAlign={{"right": "right"}}>
                            <AnchorLink router={router} href="/">Hjem</AnchorLink>
                            {newUserLink}
                            {logInLink}
                            {logOutLink}
                        </Menu>
                    </Box>
                </Header>
                <Error />
                {this.props.children}
            </GrommetApp>
        )
    }  
} 

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.user,
        userData: state.userData,
        children: ownProps.children,
        firebase: ownProps.firebase,
        router: ownProps.router
    }
}

const mapDispatchToProps = (dispatch) => {
    return({
        setErrormessage: (error) => {dispatch(setErrorMessage(error))}
    })
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App)