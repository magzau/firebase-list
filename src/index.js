import { AppContainer } from 'react-hot-loader'
import { render } from 'react-dom'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import React from 'react'

import AppRouter from './container/AppRouter'
import firebase from 'firebase'
import reducer from './reducers'

import { getFirebaseData } from './api'

const rootEl = document.getElementById('root')
const store = createStore(reducer)
const testUserActivated = false
const testUser = {uid: "testuser"}

// Initialize Firebase
firebase.initializeApp(
    {
        apiKey: "AIzaSyBFvzu-iIwlViti1h5HKg4xThML-VuncBg",
        authDomain: "huskeliste-4d3d7.firebaseapp.com",
        databaseURL: "https://huskeliste-4d3d7.firebaseio.com",
        projectId: "huskeliste-4d3d7",
        storageBucket: "huskeliste-4d3d7.appspot.com",
        messagingSenderId: "380421917816"
    }
)

firebase.auth().onAuthStateChanged((user) => {
    const userLoggedIn = user ? user : (testUserActivated ? testUser : null)
    getFirebaseData(userLoggedIn, firebase, store)
})

render(
    <Provider store={store}>
        <AppContainer>
            <AppRouter firebase={firebase}/>
        </AppContainer>
    </Provider>,
    rootEl
)

if (module.hot) {
    module.hot.accept('./container/AppRouter', () => {
        const NextApp = require('./container/AppRouter').default

        render(
            <Provider store={store}>
                <AppContainer>
                    <NextApp firebase={firebase}/>
                </AppContainer>
            </Provider>,
            rootEl
        )
    })
}

import { install, applyUpdate } from 'offline-plugin/runtime'

install({
    onUpdateReady: () => applyUpdate()
})
