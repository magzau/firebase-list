import React, { Component } from 'react'
import { History, Link } from 'react-router'
import { connect } from 'react-redux'
import Flexbox from 'flexbox-react'

import TextInput from 'grommet/components/TextInput'
import Button from 'grommet/components/Button'

import { setErrorMessage } from '../actions'
import { logIn } from '../api/user'

class LogIn extends Component {
	constructor(props) {
		super(props)

		this.state = {
			email: "",
			password: ""
		}
	}

	componentWillMount() {
		if (this.props.user !== null) {
			this.props.router.push("/")
		}
	}

	componentWillUpdate(nextProps, nextState) {
		if (nextProps.user !== null) {
			nextProps.router.push("/")
		}
	}

	_onInputChanged(event) {
		const inputName = event.target.name
		const inputValue = event.target.value
		this.setState({
			[inputName]: inputValue
		});
	}

	_onLogInClicked () {
		logIn(this.state.email, this.state.password, this.props.firebase, this.props.setErrormessage)
	}

	render(){
		const logInResult = this.state.logInResult || ""

		return (
			<Flexbox name="flexbox" flexDirection="column">
				<TextInput
					placeHolder="E-post"
					name="email"
					onDOMChange={(e) => {this._onInputChanged(e)}} />
				<TextInput
					placeHolder="Passord"
					type="password"
					name="password"
					onDOMChange={(e) => {this._onInputChanged(e)}} />
				<Button 
					onClick={() => this._onLogInClicked()}
					label="Logg Inn"/>
				<h2>
					{logInResult}
				</h2>
			</Flexbox>
		)
	} 
}

const mapStateToProps = (store, ownProps) => {
  return {
    user: store.user,
    firebase: ownProps.firebase
  }
}

const mapDispatchToProps = (dispatch) => {
    return({
        setErrormessage: (error) => {dispatch(setErrorMessage(error))}
    })
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LogIn)