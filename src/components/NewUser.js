import React, { Component } from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import Flexbox from 'flexbox-react'
import TextInput from 'grommet/components/TextInput'
import Button from 'grommet/components/Button'

import { createUser } from '../api/user'

class NewUser extends Component {
	constructor(props) {
		super(props)

		this.state = {
			email: "",
			password: ""		}
	}

	componentWillMount() {
		if (this.props.user !== null) {
			this.props.router.push("/")
		}
	}

	componentWillUpdate(nextProps, nextState) {
		if (nextProps.user !== null) {
			nextProps.router.push("/")
		}
	}

	_onInputChanged(event) {
		const inputName = event.target.name
		const inputValue = event.target.value
		
		if(inputName === 'email') {
			event.target.type = "email"
		}
		event.target.required = true

		this.setState({
			[inputName]: inputValue
		})
	}

	_missingInput() {
		return this.state.email === "" || this.state.password === ""
	}

	_invalidInput() {
		//TODO: regex and corresponding password1 and password2
		return false
	}

	_onSaveClicked() {
		if (this._missingInput() || this._invalidInput()) {
			console.log("INVALID INPUT", this.props)
			return
		}

		createUser(this.state.email, this.state.password, this.props.firebase)
	}

	render () {
		return (
			<Flexbox flexDirection="column">
				<TextInput
					placeHolder="E-post"
					name="email"
					onDOMChange={(e) => {this._onInputChanged(e)}} />
				<TextInput
					placeHolder="Passord"
					type="password"
					name="password"
					onDOMChange={(e) => {this._onInputChanged(e)}} />
				<Button 
					onClick={() => {this._onSaveClicked()}}
					label="Opprett og logg inn"/>
			</Flexbox>
		)
	}
}

const mapStateToProps = (store, ownProps) => {
  return {
    user: store.user,
    firebase: ownProps.firebase
  }
}

export default connect(
  mapStateToProps
)(NewUser)