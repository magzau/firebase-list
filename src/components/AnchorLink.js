import { connect } from 'react-redux'
import React from 'react'

import Anchor from 'grommet/components/Anchor'

const AnchorLink = ({props}) => {
	const onClick = (event) => {
		event.preventDefault()
    	props.router.push(props.href)
	}

	return (
		<Anchor
			href={props.href}
			children={props.children}
			onClick={props.onClick ? (event) => props.onClick(event) : (event) => onClick(event)} />
	)
}

const mapStateToProps = (state, ownProps) => {
  return {
    props: ownProps
  }
}

export default connect(
  mapStateToProps
)(AnchorLink)