import React from 'react'

const HomeNotLoggedIn = () => (
	<div>
		Opprett bruker eller logg inn for å bruke appen
	</div>
)

export default HomeNotLoggedIn
