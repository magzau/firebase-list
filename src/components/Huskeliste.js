import React, { Component } from 'react'
import Button from 'grommet/components/Button'
import List from 'grommet/components/List'
import ListItem from 'grommet/components/ListItem'
import TextInput from 'grommet/components/TextInput'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import Collapse, {Panel} from 'rc-collapse'

import {saveToItemList} from '../api/user'

import HomeNotLoggedIn from './HomeNotLoggedIn'

class Huskeliste extends Component {
	constructor(props) {
		super(props)

		this.state = {
			name: "",
			category: "",
			suggestions: []
		}
	}

	onNameChanged(event) {
		this.setState({
			name: event.target.value
		});
	}

	onCategoryChanged(event) {
		const input = event.target.value
		const suggestions = this.getSuggestions(input)
		this.setState({
			category: input,
			suggestions: suggestions
		});
	}

	getSuggestions(input) {
		const listItems = this.props.listItems
		console.log("ITEM", listItems)
		var categories = []
		for (var key in listItems) {
			if (listItems.hasOwnProperty(key)) {
				const item = listItems[key]
				console.log("ITEM", item)

				const category = item.category || "Annet"
				if (category.indexOf(input) !== -1 && !categories.includes(category)) {
					categories.push(category)
					console.log("CAT", category)
				}
			}
		}

		return categories
	}

	getCategories() {
		const listItems = this.props.listItems
		var categories = []
		for (var key in listItems) {
			if (listItems.hasOwnProperty(key)) {
				const item = listItems[key]
				const category = item.category || "Annet"
				if (!categories.includes(category)) {
					categories.push(category)
					console.log("CAT", category)
				}
			}
		}
		return categories
	}

	onCategorySelected(event) {
		this.setState({
			category: event.suggestion
		});
	}

	addToList() {
		const item = {
			name: this.state.name,
			category: this.state.category
		}

		saveToItemList(this.props.firebase, this.props.userData, item)

		const category = item.category || ""
		this.setState({
			name: "",
			category: category
		})
	}

	renderLists() {
		var categories = {}
		const listItems = this.props.listItems

		for (var key in listItems) {
			if (listItems.hasOwnProperty(key)) {
				const listItem = listItems[key]
				const category = listItem.category || "Annet"
				if (!categories[category]) {
					categories[category] = {items: []}
				}
				categories[category].items.push(this.renderListItem(listItem))
			}
		}

		var lists = []

		for (var key in categories) {
			if (categories.hasOwnProperty(key)) {
				const category = categories[key]
				lists.push(
					<Panel header={key + " (" + category.items.length + ")"} key={key}>
						<List>
							{category.items}
						</List>
					</Panel>
				)
			}
		}
		return lists
	}

	renderListItem(listItem) {
		const itemId = listItem.id

		return (
			<ListItem key={itemId}> 
				{listItem.name} 
			</ListItem>
		)
	}

	render () {
		if (this.props.user) {
			return (
				<div>
					<TextInput 
						placeHolder="Navn" 
						value={this.state.name}
						name="productName"
						onDOMChange={(e) => {this.onNameChanged(e)}} />
					<TextInput
						placeHolder="Liste"
						name="category"
						value={this.state.category}
						onDOMChange={(e) => {this.onCategoryChanged(e)}}
						onSelect={(e) => {this.onCategorySelected(e)}}
						suggestions={this.state.suggestions} />
					<Button label="Legg til" onClick={() => {this.addToList()}}/>
					<Collapse>
						{this.renderLists()}
					</Collapse>
				</div>
			)
		} else {
			return (
				<HomeNotLoggedIn />
			)
		}
	}
}

const mapStateToProps = (store, ownProps) => {
    const listItems = store.userData ? store.userData.listItems : {}
	
	return {
		user: store.user,
		userData: store.userData,
		listItems: listItems,
		firebase: ownProps.firebase
	}
}

export default connect(
  	mapStateToProps
)(Huskeliste)