import { connect } from 'react-redux'
import React from 'react'

import Notification from 'grommet/components/Notification'

const Error = ({errorMessage}) => {
    if (errorMessage) {
        return (
            <Notification
                message={errorMessage}
                size='small'
                closer={true} />
        )
    } else {
        return null
    }
}

const mapStateToProps = (state) => {
	return {
	  	errorMessage: state.error
	}
}

export default connect(
  	mapStateToProps
)(Error)