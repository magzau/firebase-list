import { logInAction } from '../actions'
import { getUserData } from './user'

export const getFirebaseData = (userLoggedIn, firebase, store) => {
    if (userLoggedIn) {
        getUserData(firebase, userLoggedIn, store)
    }
    store.dispatch(logInAction(userLoggedIn))
}