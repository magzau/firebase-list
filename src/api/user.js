import request from 'request'
import { logInAction, setUserData, setErrorMessage } from '../actions'
import userStatus from '../constants/userStatus'

const encodeEmail = (email) => {
    return btoa(email)
}

const getUserDataRef = (firebase, email) => {
    return firebase.database().ref('userdata/' + encodeEmail(email))
}

const getInventoryRef = (firebase, email, productId) => {
    return firebase.database().ref('userdata/' + encodeEmail(email) + "/listItems")
}

const addIdsToProducts = (firebaseList) => {
    var newList = []
    for (var key in firebaseList) {
        if (firebaseList.hasOwnProperty(key)) {
            let product = firebaseList[key]
            product.id = key
            newList.push(product)
        }
    }
    return newList
}

export const getUserData = (firebase, user, store) => {
    const userDataRef = getUserDataRef(firebase, user.email)

    userDataRef.on('value', function(snapshot) {
        var userData = snapshot.val();
        if(userData) {
            getUserLists(firebase, userData, store)
        }
    })
}

const getUserLists = (firebase, userData, store) => {
    const inventoryEmail = getInventoryEmail(userData)

    if (inventoryEmail === userData.email) {
        userData.listItems = addIdsToProducts(userData.listItems)
        store.dispatch(setUserData(userData))
    } else { //get inventory og user that invited
        const inventoryRef = getInventoryRef(firebase, inventoryEmail)

        inventoryRef.on('value', function(snapshot) {
            const userInventory = snapshot.val()
            userData.listItems = addIdsToProducts(userInventory)
            store.dispatch(setUserData(userData))
        })
    }
}

const getInventoryEmail = (userData) => {
    return userData.invitedBy ? userData.invitedBy : userData.email
}

export const saveToItemList = (firebase, userData, listItem) => {
    const inventoryEmail = getInventoryEmail(userData)
    const inventoryRef = getInventoryRef(firebase, inventoryEmail, listItem.id)
    const productId = listItem.id

    if (productId) {
        inventoryRef.child(productId).set(listItem)
    } else {
        inventoryRef.push(listItem)
    }
}

export const logIn = (email, password, firebase, setErrorMessage) => {
	firebase.auth()
		.signInWithEmailAndPassword(email, password)
		.catch(function(error) {
            setErrorMessage("Feil ved innlogging: " + error.message)
			console.log("FEIL VED INNLOGGING. FEILKODE " + error.code)
			console.log(error.message)
		})
}

export const logOut = (firebase) => {
	firebase.auth().signOut().then(function() {
		console.log("LOGGED OUT");
    }, function(error) {
        setErrorMessage("Feil ved utlogging: " + error.message)
		console.log("FEIL VED UTLOGGING. FEILKODE " + error.code);
		console.log(error.message);
    })	
}

export const createUser = (email, password, firebase) => {
	firebase.auth()
		.createUserWithEmailAndPassword(email, password)
		.catch(function(error) {
            setErrorMessage("Feil ved oppretting av bruker: " + error.message)
			console.log("FEIL VED OPPRETTING AV BRUKER. FEILKODE " + error.code);
			console.log(error.message);
	})

	getUserDataOnce(firebase, email).then(result => {
		if (result) {
            setErrorMessage("Bruker med epost " + email + " finnes allerede")
			console.log("EMAIL ALREADY ADDED: ", result)
			return
		}
		addUserData(email, firebase)
	})
}

const getUserDataOnce = (firebase, email) => {
    const userDataRef = getUserDataRef(firebase, email)

    return userDataRef.once('value').then(function(snapshot) {
        return snapshot.val()
    })
}

const addUserData = (email, firebase) => {
    const userDataRef = getUserDataRef(firebase, email)

    userDataRef.set({
        email: email
    })
}