try {
  require('os').networkInterfaces();
}
catch (e) {
  require('os').networkInterfaces = () => ({});
}

var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');

const extractCSS = new ExtractTextWebpackPlugin("styles.css");

module.exports = {
  devtool: 'eval',
  entry: [
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:3000',
    'webpack/hot/only-dev-server',
    './src/vendor',
    './src/index'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  plugins: [
    extractCSS,
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      template: 'public/index.html'
    })
  ],
  module: {
    rules: [{
      test: /\.js$/,
      use: 'babel-loader',
      include: path.join(__dirname, 'src')
    },
    {
      test: /\.css$/,
      use: extractCSS.extract({
          fallback: "style-loader",
          use: "css-loader"
      })
    }]
  },
  node: {
    net: "empty",
    tls: "empty",
    fs: "empty"
  }
};
